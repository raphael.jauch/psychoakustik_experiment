"""Runs 4 experiments determening the individual CMR with and without postcursor flanking bands.

The experiments are based on the pypercept package. For more information on its usage, 
please refere to https://gitlab.gwdg.de/ha_soft/pypercept/ (GNU-GLP)"""

import numpy as np
import scipy as sp
import pypercept
from matplotlib import pyplot as plt
from scipy import signal,io
from scipy.fft import fft,fftfreq
from pypercept.experiments import AdaptAFCExperiment, Experiment
from pypercept.order import Sequential
from pypercept.utils import gensin, rms,fft_rect_filt
import json

__author__ = "Raphael Jauch, Felix Gugel, Kai L. Lutz"
__credits__ = ["Raphael Jauch, Felix Gugel, Kai L. Lutz"]
__license__ = "GPL"
__version__ = "1.0"
__email__ = "raphael.jauch@student.jade-hs.de"
__status__ = "Production"

class BaseExperiment(Experiment, Sequential):
    
    def init_experiment(self, exp):
        exp.description = """
        This experiment measures the threshold......
        """ + exp.description
        exp.num_afc = 3 # The number of AFC alternatives
        exp.sample_rate = 48000 #sample rate of all signals
        exp.sine_freq = 1000 #frequency of cue sinoid
        exp.noise_width = 20 #band width of masker and flanking bands

        ##### hier drann drehen für kalibrierung wenn nötig #######
        exp.noise_level = -30 #noise level (dB FS) -> adjust till noise is 60dB SPL

        exp.debug_plot_signal = True 

        exp.signal_gap_duration = 0.0625 #pause inbetween masker and postcursors in [s]
        exp.signal_duration = 0.1875 #duration of signal/masker noise/postcursor in [s]
        exp.masker_ramp_duration = 0.02 #duration of raised cosine ramps applied to masker/prostcursor in [s]

        exp.isi_signal = 0.3 # Inter-Stimulus-Interval, duration of silence in s
        exp.pre_signal = 0.3 # pre signal interval, duration of silence in s
        exp.post_signal = 0.2 # post signal interval, duration of silence in s

        exp.show_plots=False # debug variable, show plots of different signal parts
        
        #load calibration data for narrow band noises
        with open('2024-12-06 - 15-57-40.json','r')as inFile:
            exp.calibration_dict = json.load(inFile)
        
        #start sinus level, to be adaptivly controlled
        exp.set_variable('sine_level', float(exp.calibration_dict['1000']), 'dB', 'level of sinusoid test signal') 
        #flanking band center frequency configurations (broadband / narrowband)
        exp.add_parameter('flanking_band_freqs', [(250,500,1000,2000,4000), (794,891,1000,1123,1260)], 'Hz', 'flanking band center frequencies')
        #adaptive methode for threshold ....
        exp.add_adapt_setting('1up2down',max_reversals = 8, start_step = 8, min_step=2)


    def init_run(self,run):
        '''The durations of sine, ramp, pause between noise and cue
        and silence after reference signal, in seconds'''

        # The same durations in number of samples:
        run.sine_nsamp = int(np.round(self.signal_duration * run.sample_rate))
        run.ramp_nsamp = int(np.round(self.masker_ramp_duration * run.sample_rate))
        run.noise_nsamp = int(np.round(self.signal_duration* run.sample_rate))
        run.gap_nsamp = int(np.round(self.signal_gap_duration * run.sample_rate))

        # Pre-generate the sinusoid cue with RMS = 1
        run.sine = gensin(self.sine_freq, np.sqrt(2), self.signal_duration, run.sample_rate)

        #half oscilation of raised cosine
        run.cosine = gensin(freq=1/(2*self.masker_ramp_duration),ampl=0.5,length_sec=self.masker_ramp_duration,phase_deg=270)+0.5
        if self.show_plots:
            plt.plot(run.cosine)
            plt.show()

        #raised cosine ramp window
        run.cosine_window = np.concatenate((run.cosine,np.ones(run.noise_nsamp-2*run.ramp_nsamp),np.flip(run.cosine)))
        if self.show_plots:
            plt.plot(run.cosine_window)
            plt.show()

        #vector containing zeros for gap between signal and postcursors
        run.gap_vector = np.zeros(run.gap_nsamp)


    def init_trial(self, trial, run):
        pass



class IndependentFlanks(BaseExperiment, AdaptAFCExperiment):

    def init_experiment(self, exp):
        exp.description = """
        Experiment to determin the detection threshold of a sine, masked by a 20Hz noise while playing
        4 flanking band noises, in a broad configuration (flanking bands centered at 250, 500, 2000 and 4000 Hz)
        and in a narrow band (flanking bands centered at 794, 891, 1123 and 1260 Hz); the masker and flanking noise
        are independently sampled from an gausian distribution.
        """
        exp.subject_name = name_prefix + 'Independent'
        super().init_experiment(exp)

    def init_trial(self, trial, run):
        # Generate num_afc * 5 new, independent realisations of Gaussian noise
        noise_nsamp = int(np.round(self.signal_duration * run.sample_rate))
        noise = np.random.randn( noise_nsamp, self.num_afc, len(run.flanking_band_freqs) )
        presentation_signal = []
        if self.show_plots:
            fig, axs = plt.subplots(ncols=self.num_afc,constrained_layout=True)
            fig2, axs2 = plt.subplots(nrows=self.num_afc,constrained_layout=True)
        # filter noise to narrow band
        for k in range(self.num_afc):
            presentation_signal.append(np.zeros(noise_nsamp+run.gap_nsamp*2))
            for j in range(len(run.flanking_band_freqs)):

                noise[:,k,j] = fft_rect_filt(noise[:,k,j],run.flanking_band_freqs[j]-(self.noise_width/2),run.flanking_band_freqs[j]+(self.noise_width/2),
                              run.sample_rate)
                #window each band 
                noise[:,k,j] *= run.cosine_window
                #adjust level to 60dB in each band
                noise[:,k,j] = noise[:,k,j] / rms(noise[:,k,j]) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)

                #add sine to 1kHz band of first signal
                if k == 0 and j == 2:
                    sine_adjusted = (run.sine * 10**(trial.variable/20))
                    sine_adjusted *= run.cosine_window
                    noise[:,k,j] += sine_adjusted

                #add silence at begin and end of signal
                current_noise = np.concatenate((run.gap_vector,noise[:,k,j],run.gap_vector))
                #add to presentation signal
                presentation_signal[k] += current_noise
            
            # plot signal and spectrum if debug variable is set
            if self.show_plots:
                emptySigSpectrum = fft(presentation_signal[k])
                emptySigSpectrumX = fftfreq(len(presentation_signal[k]),1/run.sample_rate)
                axs[k].plot(emptySigSpectrumX,20*np.log10(np.absolute(emptySigSpectrum)))
                axs[k].set_xlim([run.flanking_band_freqs[0]-200,run.flanking_band_freqs[-1]+200])
                axs[k].set_ylim([-10,50])
                axs[k].set_xlabel('Frequency [Hz]')
                axs[k].set_ylabel('Amplitude [dB]')
                axs2[k].plot(presentation_signal[k])
        
        if self.show_plots:
            plt.show()
        #return cue signal and non-cue signal
        trial.test_signal = presentation_signal[0]
        trial.reference_signal = presentation_signal[1:] 

        super().init_trial(trial,run)

    

class ComodulatedFlanks(BaseExperiment, AdaptAFCExperiment):

    def init_experiment(self, exp):
        exp.description = """
        Experiment to determin the detection threshold of a sine, masked by a 20Hz noise while playing
        4 flanking band noises, in a broad configuration (flanking bands centered at 250, 500, 2000 and 4000 Hz)
        and in a narrow band (flanking bands centered at 794, 891, 1123 and 1260 Hz); the masker and flanking noise
        are comodulated.
        """
        exp.subject_name = name_prefix + 'Comodulated'
        super().init_experiment(exp)

    def init_trial(self, trial, run):
        # number of samples in one interval
        noise_nsamp = int(np.round(self.signal_duration * run.sample_rate))

        presentation_signal = []
        #generate 3 signal
        for k in range(self.num_afc):
            #init the presentation signal with zeros
            presentation_signal.append(np.zeros(noise_nsamp+run.gap_nsamp*2))
            current_signal = np.zeros(noise_nsamp)
            #sample masker noise and extract narrow band around 1000 Hz
            noise_center = fft_rect_filt(np.random.randn(noise_nsamp),1000-(self.noise_width/2),1000+(self.noise_width/2),run.sample_rate)
            #transform masker noise to spectrum
            ffty = sp.fft.fft(noise_center)
            fftx = sp.fft.fftfreq(noise_nsamp,1/run.sample_rate)
            #plot spectrum if debug variable is set
            if self.show_plots:
                plt.plot(fftx,20*np.log10(np.absolute(ffty)))
                plt.show()

            # apply cosine ramp to masker
            noise_center *= run.cosine_window
            # set sound level of masker to 60 dB SPL by using calibration data (-20dB since calibration
            # was performed to 80 dB SPL)
            noise_center = noise_center / rms(noise_center) * 10**((self.calibration_dict['1000']-20)/20)
            # add masker to singal
            current_signal += noise_center

            # add sine to first signal of the 3 AFC experiment
            if k == 0:
                sine_adjusted = (run.sine * 10**(trial.variable/20))
                sine_adjusted *= run.cosine_window
                current_signal += sine_adjusted

            #generate flanking bands
            for j in range(len(run.flanking_band_freqs)):
                #skip flanking band at 1000 Hz
                if j==2:
                    continue
                # split spectrum and shift towards target center frequency
                positiv_half = np.roll(ffty[int(np.round(len(ffty)/2)):len(ffty)],int(-np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                negative_half = np.roll(ffty[:int(np.round(len(ffty)/2)-1)],int(np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                noise_flank_spectrum = np.concatenate((negative_half,[0],positiv_half))
                # transform spectrum back to time domain, apply cosine ramp
                flank = sp.fft.ifft(noise_flank_spectrum)*run.cosine_window
                # set sound level of flanking band to 60 dB SPL by using calibration data (-20dB since calibration
                # was performed to 80 dB SPL)
                flank = flank / rms(flank) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)
                # add flanking band to current signal
                current_signal +=np.real(flank)
                # plot if debug variable is set
                if self.show_plots:
                    plt.plot(fftx,20*np.log10(np.absolute(noise_flank_spectrum)))
                    plt.show()

            # add silence to start and end of signal
            presentation_signal[k] = np.concatenate((run.gap_vector,current_signal,run.gap_vector))

        # set the test and reference signal of pypercept
        trial.test_signal = presentation_signal[0]
        trial.reference_signal = presentation_signal[1:] 

        super().init_trial(trial,run)
            
                

class IndependentFlanksPostcursor(BaseExperiment, AdaptAFCExperiment):

    def init_experiment(self, exp):
        exp.description = """
        Experiment to determin the detection threshold of a sine, masked by a 20Hz noise while playing
        4 flanking band noises, followed by 4 intervals of the flanking bands, in a broad configuration 
        (flanking bands centered at 250, 500, 2000 and 4000 Hz) and in a narrow band (flanking bands centered 
        at 794, 891, 1123 and 1260 Hz); the masker and flanking noise are independently sampled from an 
        gausian distribution.
        """
        exp.subject_name = name_prefix + 'IndependentPostcursor'
        super().init_experiment(exp)

    def init_trial(self, trial, run):
            # length of one interval with silence added
            noise_nsamp = int(np.round(self.signal_duration * run.sample_rate))
            # Generate num_afc * 5 new, independent realisations of Gaussian noise
            noise = np.random.randn( noise_nsamp, self.num_afc, len(run.flanking_band_freqs) )
            presentation_signal = []
            
            # initialize plot if debug variable is set
            if self.show_plots:
                fig, axs = plt.subplots(ncols=self.num_afc,constrained_layout=True)
                fig2, axs2 = plt.subplots(nrows=self.num_afc,constrained_layout=True)
            # generate 3 signals
            for k in range(self.num_afc):
                # initialize current signal with zeros 
                presentation_signal.append(np.zeros(run.gap_nsamp*6+noise_nsamp*5))
                # generate masker/flanking noises
                for j in range(len(run.flanking_band_freqs)):
                    # sample gausian noise and filter to 20Hz narrow band centered at current frequency
                    noise[:,k,j] = fft_rect_filt(noise[:,k,j],run.flanking_band_freqs[j]-(self.noise_width/2),run.flanking_band_freqs[j]+(self.noise_width/2),
                                run.sample_rate)
                    #window each band 
                    noise[:,k,j] *= run.cosine_window
                    # set sound level of masker/flanking band to 60 dB SPL by using calibration data (-20dB since calibration
                    # was performed to 80 dB SPL)
                    noise[:,k,j] = noise[:,k,j] / rms(noise[:,k,j]) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)

                    #add sine to 1kHz band of first signal
                    if k == 0 and j == 2:
                        sine_adjusted = (run.sine * 10**(trial.variable/20))
                        sine_adjusted *= run.cosine_window
                        noise[:,k,j] += sine_adjusted

                    #add silence at begin and end of signal
                    current_noise = np.concatenate((run.gap_vector,noise[:,k,j]))

                    #generate postcursor noises (if current band is not 1000Hz)
                    if j != 2:
                        #sample gausian 4 times for 4 intervals of postcursor
                        post_noise = np.random.randn( noise_nsamp,4)
                        for i in range(4):
                            # filter postcursor noise to narrow band around current center frequency
                            post_noise[:,i] = fft_rect_filt(post_noise[:,i],run.flanking_band_freqs[j]-(self.noise_width/2),run.flanking_band_freqs[j]+(self.noise_width/2),
                                run.sample_rate)
                            # apply cosine ramp
                            post_noise[:,i] *= run.cosine_window
                            # set sound level of postcursor to 60 dB SPL by using calibration data (-20dB since calibration
                            # was performed to 80 dB SPL)
                            post_noise[:,i] = post_noise[:,i] / rms(post_noise[:,i]) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)
                            #append postcursor noise
                            current_noise = np.concatenate((current_noise,run.gap_vector,post_noise[:,i]))
                    else:
                        current_noise = np.concatenate((current_noise,np.zeros((run.gap_nsamp+noise_nsamp)*4)))
                    #add to presentation signal
                    presentation_signal[k] += np.concatenate((current_noise,run.gap_vector))
                
                # plot if debug variable is set
                if self.show_plots:
                    emptySigSpectrum = fft(presentation_signal[k])
                    emptySigSpectrumX = fftfreq(len(presentation_signal[k]),1/run.sample_rate)
                    axs[k].plot(emptySigSpectrumX,20*np.log10(np.absolute(emptySigSpectrum)))
                    axs[k].set_xlim([run.flanking_band_freqs[0]-200,run.flanking_band_freqs[-1]+200])
                    axs[k].set_ylim([-10,50])
                    axs[k].set_xlabel('Frequency [Hz]')
                    axs[k].set_ylabel('Amplitude [dB]')
                    axs2[k].plot(presentation_signal[k])
            
            if self.show_plots:
                plt.show()
            #return cue signal and non-cue signal to pypercept
            trial.test_signal = presentation_signal[0]
            trial.reference_signal = presentation_signal[1:] 

            super().init_trial(trial,run)

class ComodulatedFlanksPostcursor(BaseExperiment, AdaptAFCExperiment):

    def init_experiment(self, exp):
        exp.description = """
        Experiment to determin the detection threshold of a sine, masked by a 20Hz noise while playing
        4 flanking band noises, followed by 4 intervals of the flanking bands, in a broad configuration 
        (flanking bands centered at 250, 500, 2000 and 4000 Hz) and in a narrow band (flanking bands centered 
        at 794, 891, 1123 and 1260 Hz); the masker and flanking noise are comodulated.
        """
        exp.subject_name = name_prefix + 'ComodulatedPostcursor'
        super().init_experiment(exp)


    def init_trial(self, trial, run):
        # sample count of noise + gap between noises
        noise_nsamp = int(np.round(self.signal_duration * run.sample_rate))

        presentation_signal = []
        #generate 3 signales for 3 AFC experiment
        for k in range(self.num_afc):
            #init current signal with zerose
            current_signal = np.zeros(noise_nsamp)
            #sample gausian and filter to 20Hz narrow band centered a 100ß Hz for masker noise
            noise_center = fft_rect_filt(np.random.randn(noise_nsamp),1000-(self.noise_width/2),1000+(self.noise_width/2),run.sample_rate)
            #calculate spectrum of center noise
            ffty = sp.fft.fft(noise_center)
            fftx = sp.fft.fftfreq(noise_nsamp,1/run.sample_rate)
            #plot if debug variable is set
            if self.show_plots:
                plt.plot(fftx,20*np.log10(np.absolute(ffty)))
                plt.show()
            #apply cosine ramp to masker noise
            noise_center *=run.cosine_window
            # set sound level of masker to 60 dB SPL by using calibration data (-20dB since calibration
            # was performed to 80 dB SPL)
            noise_center = noise_center / rms(noise_center) * 10**((self.calibration_dict['1000']-20)/20)
            #add masker to current signal
            current_signal += noise_center

            #generate cue signal for first signal of 3 AFC experiment
            if k == 0:
                #adjust sine level, apply cosine ramp
                sine_adjusted = (run.sine * 10**(trial.variable/20))
                sine_adjusted *= run.cosine_window
                #add sine to current signal
                current_signal += sine_adjusted

            #generate flanking bands
            for j in range(len(run.flanking_band_freqs)):
                #skip if current band is 1000 Hz
                if j==2:
                    continue
                #split spectrum into positiv/negativ halves, shift spectrum towards target frequency
                positiv_half = np.roll(ffty[int(np.round(len(ffty)/2)):len(ffty)],int(-np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                negative_half = np.roll(ffty[:int(np.round(len(ffty)/2)-1)],int(np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                noise_flank_spectrum = np.concatenate((negative_half,[0],positiv_half))
                #transform spectrum to time domain, apply cosine ramp
                flank = sp.fft.ifft(noise_flank_spectrum)*run.cosine_window
                # set sound level of flanking to 60 dB SPL by using calibration data (-20dB since calibration
                # was performed to 80 dB SPL)
                flank = flank / rms(flank) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)
                #add flanking band to current signal
                current_signal +=np.real(flank)
                #plot if debug varibale is set
                if self.show_plots:
                    plt.plot(fftx,20*np.log10(np.absolute(noise_flank_spectrum)))
                    plt.show()
            #init postcursor variable
            postcursor = np.zeros((noise_nsamp+run.gap_nsamp)*4)
            #generate 4 postcursor intervals
            for i in range(4):
                #generate center noise, filter to narrow band
                centerband_postcursor = fft_rect_filt(np.random.randn(noise_nsamp),1000-(self.noise_width/2),1000+(self.noise_width/2),run.sample_rate)
                #transform center nosie to frequency domain
                ffty_postcursor = sp.fft.fft(noise_center)
                fftx_postcursor = sp.fft.fftfreq(noise_nsamp,1/run.sample_rate)
                #plot if debug variable is set
                if self.show_plots:
                    plt.plot(fftx,20*np.log10(np.absolute(ffty)))
                    plt.show()

                #init helper variable holding single intveral of flanking bands
                allbands = np.zeros(noise_nsamp+run.gap_nsamp)
                #generate flanking bands
                for j in range(len(run.flanking_band_freqs)):
                    #skip if current band is 1000Hz
                    if j==2:
                        continue
                    #split spectrum in half, shift towards target center frequency
                    positiv_half = np.roll(ffty_postcursor[int(np.round(len(ffty_postcursor)/2)):len(ffty_postcursor)],int(-np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                    negative_half = np.roll(ffty_postcursor[:int(np.round(len(ffty_postcursor)/2)-1)],int(np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                    noise_flank_spectrum = np.concatenate((negative_half,[0],positiv_half))
                    #transform to time domain, apply cosine ramp
                    flank = sp.fft.ifft(noise_flank_spectrum)*run.cosine_window
                    # set sound level of flaking band to 60 dB SPL by using calibration data (-20dB since calibration
                    # was performed to 80 dB SPL)
                    flank = flank / rms(flank) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)
                    #add current postcursor band to helper variable
                    allbands +=np.concatenate((run.gap_vector,np.real(flank)))
                #add current postcursor interval to postcursor variable
                postcursor += np.concatenate((np.zeros((noise_nsamp+run.gap_nsamp)*i),allbands,np.zeros((noise_nsamp+run.gap_nsamp)*(3-i))))

            #concatenate masker interval with postcursor interval
            presentation_signal.append(np.concatenate((run.gap_vector,current_signal,postcursor,run.gap_vector)))

        #return cue and reference signals to pypercept
        trial.test_signal = presentation_signal[0]
        trial.reference_signal = presentation_signal[1:] 

        super().init_trial(trial,run)

class ComodulatedFlanksPostcursorGapped(BaseExperiment, AdaptAFCExperiment):

    def init_experiment(self, exp):
        exp.description = """
        Experiment to determin the detection threshold of a sine, masked by a 20Hz noise while playing
        4 flanking band noises, followed by 1 interval of silence and 3 intervals of the flanking bands, 
        in a broad configuration (flanking bands centered at 250, 500, 2000 and 4000 Hz) and in a narrow 
        band (flanking bands centered at 794, 891, 1123 and 1260 Hz); the masker and flanking noise are comodulated.
        """
        exp.subject_name = name_prefix + 'ComodulatedPostcursorGapped'
        super().init_experiment(exp)


    def init_trial(self, trial, run):
        # sample count of masker noise
        noise_nsamp = int(np.round(self.signal_duration * run.sample_rate))

        presentation_signal = []
        #generate 3 signals for 3 AFC experiment
        for k in range(self.num_afc):
            #init current signal helper variable with zeros
            current_signal = np.zeros(noise_nsamp)
            #sample gausian noise, filter to 20 Hz narrow band centered at 1000 Hz
            noise_center = fft_rect_filt(np.random.randn(noise_nsamp),1000-(self.noise_width/2),1000+(self.noise_width/2),run.sample_rate)
            #transform to frequency domain
            ffty = sp.fft.fft(noise_center)
            fftx = sp.fft.fftfreq(noise_nsamp,1/run.sample_rate)
            #plot spectrum if debug variable is set
            if self.show_plots:
                plt.plot(fftx,20*np.log10(np.absolute(ffty)))
                plt.show()

            #apply cosine window to masker noise
            noise_center *= run.cosine_window
            # set sound level of masker to 60 dB SPL by using calibration data (-20dB since calibration
            # was performed to 80 dB SPL)
            noise_center = noise_center / rms(noise_center) * 10**((self.calibration_dict['1000']-20)/20)
            # add masker to current interval helper variable
            current_signal += noise_center

            #add sine to first signal of 3 AFC experiment
            if k == 0:
                sine_adjusted = (run.sine * 10**(trial.variable/20))
                sine_adjusted *= run.cosine_window
                current_signal += sine_adjusted

            #generate flanking bands
            for j in range(len(run.flanking_band_freqs)):
                #skip if current center frequency is 1000 Hz
                if j==2:
                    continue
                #split spectrum of masker, shift towards target center frequency
                positiv_half = np.roll(ffty[int(np.round(len(ffty)/2)):len(ffty)],int(-np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                negative_half = np.roll(ffty[:int(np.round(len(ffty)/2)-1)],int(np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                noise_flank_spectrum = np.concatenate((negative_half,[0],positiv_half))
                #transform to time domain, apply cosine ramp
                flank = sp.fft.ifft(noise_flank_spectrum)*run.cosine_window
                # set sound level of flanking band to 60 dB SPL by using calibration data (-20dB since calibration
                # was performed to 80 dB SPL)
                flank = flank / rms(flank) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)
                #add flanking band to current signal helper variable
                current_signal +=np.real(flank)
                #plot if debug variable is set
                if self.show_plots:
                    plt.plot(fftx,20*np.log10(np.absolute(noise_flank_spectrum)))
                    plt.show()

            #init postcursor helper variable for 4 postcursor intervals
            postcursor = np.zeros((noise_nsamp+run.gap_nsamp)*4)
            #gen postcursors
            for i in range(4):
                #init allbands helper variable for single postcursor interval
                allbands = np.zeros(noise_nsamp+run.gap_nsamp)
                #leave first postcursor period empty
                if i != 0:
                    #generate 20Hz narrow band noise centered at 1000Hz
                    centerband_postcursor = fft_rect_filt(np.random.randn(noise_nsamp),1000-(self.noise_width/2),1000+(self.noise_width/2),run.sample_rate)
                    #transform to frequency domain
                    ffty_postcursor = sp.fft.fft(noise_center)
                    fftx_postcursor = sp.fft.fftfreq(noise_nsamp,1/run.sample_rate)
                    #plot spectrum if debug variable is set
                    if self.show_plots:
                        plt.plot(fftx,20*np.log10(np.absolute(ffty)))
                        plt.show()

                    #generate flanking bands
                    for j in range(len(run.flanking_band_freqs)):
                        #skip 1000 Hz
                        if j==2:
                            continue
                        #split spectrum in positiv and negativ half, shift towards target center frequency
                        positiv_half = np.roll(ffty_postcursor[int(np.round(len(ffty_postcursor)/2)):len(ffty_postcursor)],int(-np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                        negative_half = np.roll(ffty_postcursor[:int(np.round(len(ffty_postcursor)/2)-1)],int(np.round((run.flanking_band_freqs[j]-1000)/(run.sample_rate/noise_nsamp))))
                        noise_flank_spectrum = np.concatenate((negative_half,[0],positiv_half))
                        #transform to time domain, apply cosine ramp
                        flank = sp.fft.ifft(noise_flank_spectrum)*run.cosine_window
                        # set sound level of flanking band to 60 dB SPL by using calibration data (-20dB since calibration
                        # was performed to 80 dB SPL)
                        flank = flank / rms(flank) * 10**((self.calibration_dict[str(run.flanking_band_freqs[j])]-20)/20)
                        #add current flanking band to allbands helper variable
                        allbands +=np.concatenate((run.gap_vector,np.real(flank)))

                #concatenate the 4 postcursor intervals
                postcursor += np.concatenate((np.zeros((noise_nsamp+run.gap_nsamp)*i),allbands,np.zeros((noise_nsamp+run.gap_nsamp)*(3-i))))

            #concatenate masker and postcursor intervals
            presentation_signal.append(np.concatenate((run.gap_vector,current_signal,postcursor,run.gap_vector)))

        #return cue and reference signal to pypercept
        trial.test_signal = presentation_signal[0]
        trial.reference_signal = presentation_signal[1:] 

        super().init_trial(trial,run)


#run all experiments sucessivly
if __name__ == '__main__':
    name_prefix = input("Bitte Initialien eingeben:")

    pypercept.start(IndependentFlanks)
    pypercept.start(IndependentFlanksPostcursor)
    pypercept.start(ComodulatedFlanks)
    pypercept.start(ComodulatedFlanksPostcursor)
    pypercept.start(ComodulatedFlanksPostcursorGapped)